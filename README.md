# README #

### What is this App for? ###

* WebApp Chooser: Lets the user navigate between 4 predefined web-apps.
      1. First web-app displays the Battery level and whether the phone is currently charging or not.
      2. Second web-app shows a static value mockin the Gps Coordinates.
      3. Third web-app shows the X,Y, Z coordinates of the phone's accelerometer.
      4. Fourth web-app consists of a button to ask the phone to vibrate.
* The three firsts apps retrieve data from the phone each second. It's the phone's responsibility the data string format which will be displayed

### How do I get set up? ###

* When running, the web-app will start retrieving info from the phone every second. Everything will be automatic and it will scroll down when reached the bottom of the screen.
* **Edit 07.05.2017:** The fragments are now stored in the Activity and will keep working even if they are not displayed, once they are started by the user.

### How ist this done? ###

* The app tries to follow the Clean Architecture for android ([more info](https://github.com/android10/Android-CleanArchitecture))
* The App is divided in three layers:
      1. Data layer with a class for each kind of data to retrieve from the phone
      2. Domain layer with a class that handles this data in order to be decouple with the rest of the project
      3. View layer with all the classes required by Android for the proper visualization of the project
* **Edit 07.05.2017:** No external, third party libraries was used:
      1. The fragments are dealt with in the Activity, where they are also stored. Principally in the MainActivity by a function to display fragments and overwritting the onBackButton function (adding a parent class was an option, but due to the relative simplicity of the functions required, this idea was at the end ruled out).
      2. Following the 'separation of concerns' principle, the handling of the fragments is done in the FragmentsHelper class.
      3. There is only one instance of the fragments in the activity and a custom Stack was implemented in FragmentsStackList. The reasoning is simple: Android does not let, by default, adding the same fragments twice:" IllegalStateException: Fragment already added". However I find important that, if the user goes: Fragment A -> B -> A -> C, the back stack should be exactly the same one, inverted: C -> A -> B -> A. This is just a personal choice.
      4. Please note that setRetainInstance(true) is not an opcion for the Fragments in this project; they all have a Context object which would produce a huge leak when recreating the activity.
      5. The Activity does not survive the Configuration Change events. That is done on purpose, following Android recommendations. Should the data-survival be a requirement, I would add persistency and reload it, rather than other uglier methods.
      6. The decision of using a Bottom Bar was taken in order to emulate the UI in some GPS devices, since it is a good and simple way to change the screen; rather than following any iOS design pattern. 
      7. The new custom view called 'FragmentChooserBar' substitutes the previous external library. There my way of coding custom classes can be checked.

* This app follows the MVP pattern: due to the characteristics of the project, this was found fitter as the MVVM. Data bindings is also not included since the android UI is fairly simple.
* To my shame, no tests were included. Had I had the time, I'd added at least unit tests for the presenters and the data repository.

* **Edit 07.05.2017:** The App's increased size (event though there are no 3rd Party libraries now) is a consequence of including the different versions of the icons in the BottomBar directly, instead of having Android generate them. Again, this was just a personal choice due to the fact that the size of the app is still not considered 'big'.