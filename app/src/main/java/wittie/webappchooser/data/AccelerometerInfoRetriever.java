package wittie.webappchooser.data;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.Size;

public class AccelerometerInfoRetriever implements SensorEventListener {

    private SensorManager sensorManager;

    private float accelerometerX;
    private float accelerometerY;
    private float accelerometerZ;

    public AccelerometerInfoRetriever(Context context) {
        registerAccelerometerListener(context);
    }

    @Override
    protected void finalize() throws Throwable {
        sensorManager.unregisterListener(this);
        super.finalize();
    }

    private void registerAccelerometerListener(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        final Sensor accelerometer  = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public @Size(3) float[] getAccInfo() {
        return new float[] {accelerometerX, accelerometerY, accelerometerZ};
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accelerometerX = sensorEvent.values[0];
            accelerometerY = sensorEvent.values[1];
            accelerometerZ = sensorEvent.values[2];
        }
    }

}
