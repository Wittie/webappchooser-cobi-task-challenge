package wittie.webappchooser.data;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.annotation.NonNull;

public class BatteryInfoRetriever {

    private final Context context;

    public BatteryInfoRetriever(Context context) {
        this.context = context;
    }

    @NonNull
    private Intent getBatteryStatus() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        final Intent batteryStatus = this.context.registerReceiver(null, intentFilter);

        if (batteryStatus == null) {
            throw new IllegalStateException("Error getting the battery level: battery status impossible to receive");
        }

        return batteryStatus;
    }

    public float getBatteryLevel() {
        final Intent batteryStatus = getBatteryStatus();
        final int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        final int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        return level / (float)scale;
    }

    public boolean isPhoneCharging() {
        final Intent batteryStatus = getBatteryStatus();
        final int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        return status == BatteryManager.BATTERY_STATUS_CHARGING;
    }

}
