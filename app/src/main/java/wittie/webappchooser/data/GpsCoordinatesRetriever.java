package wittie.webappchooser.data;

import android.support.annotation.Size;

public class GpsCoordinatesRetriever {

    public @Size(2) double[] getGpsCoordinates() {
        return new double[] {43.81518, -17.02714};
    }

}
