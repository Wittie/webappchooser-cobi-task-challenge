package wittie.webappchooser.domain;

import android.content.Context;
import android.support.annotation.Size;

import wittie.webappchooser.data.AccelerometerInfoRetriever;
import wittie.webappchooser.data.BatteryInfoRetriever;
import wittie.webappchooser.data.GpsCoordinatesRetriever;

public class DataRepository {

    private final BatteryInfoRetriever batteryInfoRetriever;
    private final GpsCoordinatesRetriever gpsCoordinatesRetriever;
    private final AccelerometerInfoRetriever accelerometerInfoRetriever;

    public DataRepository(Context context) {
        batteryInfoRetriever = new BatteryInfoRetriever(context);
        gpsCoordinatesRetriever = new GpsCoordinatesRetriever();
        accelerometerInfoRetriever = new AccelerometerInfoRetriever(context);
    }

    public float getBatteryLevel() {
        return batteryInfoRetriever.getBatteryLevel();
    }

    public boolean isPhoneCharging() {
        return batteryInfoRetriever.isPhoneCharging();
    }

    public @Size(2) double[] getGpsCoordinates() {
        return gpsCoordinatesRetriever.getGpsCoordinates();
    }

    public @Size(3) float[] getAccInfo() {
        return accelerometerInfoRetriever.getAccInfo();
    }

}
