package wittie.webappchooser.presentation.presenter;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import java.util.Locale;

import wittie.webappchooser.R;
import wittie.webappchooser.domain.DataRepository;

public class AccPresenter implements Presenter {

    private Context context;
    private final DataRepository dataRepository;

    public AccPresenter(Context context) {
        this.context = context;
        this.dataRepository = new DataRepository(context);
    }

    @Override
    public void OnPageFinishedLoading(final WebView webView, String url) {
        webView.setVisibility(View.VISIBLE);
    }

    @Override
    public String getDeviceInfo() {
        final float[] accInfo = dataRepository.getAccInfo();
        return String.format(Locale.getDefault(),
                context.getString(R.string.string_format_accelerometer),
                accInfo[0], accInfo[1], accInfo[2]);
    }

    @Override
    public void showDebugToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
