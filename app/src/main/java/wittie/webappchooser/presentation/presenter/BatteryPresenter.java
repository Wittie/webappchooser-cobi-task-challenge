package wittie.webappchooser.presentation.presenter;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import java.util.Locale;

import wittie.webappchooser.R;
import wittie.webappchooser.domain.DataRepository;

public class BatteryPresenter implements Presenter {

    private Context context;
    private final DataRepository dataRepository;

    public BatteryPresenter(Context context) {
        this.context = context;
        this.dataRepository = new DataRepository(context);
    }

    @Override
    public void OnPageFinishedLoading(final WebView webView, String url) {
        webView.setVisibility(View.VISIBLE);
    }

    @Override
    public String getDeviceInfo() {
        final int batteryLevelPercentage = (int) (dataRepository.getBatteryLevel() * 100);
        return String.format(Locale.getDefault(),
                context.getString(R.string.string_format_battery),
                batteryLevelPercentage, dataRepository.isPhoneCharging());
    }

    @Override
    public void showDebugToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
