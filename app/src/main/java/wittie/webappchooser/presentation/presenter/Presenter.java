package wittie.webappchooser.presentation.presenter;

import android.webkit.WebView;

public interface Presenter {

    void showDebugToast(String message);

    void OnPageFinishedLoading(WebView view, String url);

    String getDeviceInfo();

}
