package wittie.webappchooser.presentation.presenter;

import android.content.Context;
import android.os.Vibrator;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

public class VibrationPresenter implements Presenter {

    private Context context;

    public VibrationPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void OnPageFinishedLoading(final WebView webView, String url) {
        webView.setVisibility(View.VISIBLE);
    }

    @Override
    public String getDeviceInfo() {
        return "no info";
    }

    public void makePhoneVibrate() {
        Vibrator v = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);
    }

    @Override
    public void showDebugToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
