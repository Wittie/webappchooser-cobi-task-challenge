package wittie.webappchooser.presentation.view;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import wittie.webappchooser.R;
import wittie.webappchooser.presentation.view.fragment.AccFragment;
import wittie.webappchooser.presentation.view.fragment.BatteryFragment;
import wittie.webappchooser.presentation.view.fragment.GpsFragment;
import wittie.webappchooser.presentation.view.fragment.VibrationFragment;

public class FragmentsHelper {

    private BatteryFragment batteryFragment = new BatteryFragment();
    private AccFragment accFragment = new AccFragment();
    private GpsFragment gpsFragment = new GpsFragment();
    private VibrationFragment vibrationFragment = new VibrationFragment();
    private Fragment[] fragments = new Fragment[]{
            batteryFragment, accFragment, gpsFragment, vibrationFragment
    };

    public void hideAllFragments(FragmentTransaction fragmentTransaction) {
        final int size = fragments.length;
        for (int i = 0; i < size; i++) {
            fragmentTransaction.hide(fragments[i]);
        }
    }

    @NonNull
    public Fragment getFragmentForTabResId(@IdRes int tabId) {
        switch (tabId) {
            case R.id.tab_battery:
                return batteryFragment;
            case R.id.tab_location:
                return gpsFragment;
            case R.id.tab_acc:
                return accFragment;
            case R.id.tab_vibration:
                return vibrationFragment;
            default:
                throw new IllegalStateException("Unexpected Index for tabId: " + tabId);
        }
    }

    @IdRes
    public int getResIdForFragmentWithName(String topFragmentName) {

        if (BatteryFragment.class.getName().equals(topFragmentName)) {
            return R.id.tab_battery;
        }

        if (GpsFragment.class.getName().equals(topFragmentName)) {
            return R.id.tab_location;
        }

        if (AccFragment.class.getName().equals(topFragmentName)) {
            return R.id.tab_acc;
        }

        if (VibrationFragment.class.getName().equals(topFragmentName)) {
            return R.id.tab_vibration;
        }

        throw new IllegalStateException("Unexpected Fragment Name: " + topFragmentName);
    }

    @Nullable
    public Fragment getFragmentWithName(String name) {
        final int size = fragments.length;
        for (int i = 0; i < size; i++) {
            if (fragments[i].getClass().getName().equals(name)) {
                return fragments[i];
            }
        }

        return null;
    }
}
