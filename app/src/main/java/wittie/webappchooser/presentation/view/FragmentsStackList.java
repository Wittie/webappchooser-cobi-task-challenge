package wittie.webappchooser.presentation.view;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

/**
 * Created by plopez on 07/05/17.
 */

public class FragmentsStackList extends ArrayList<String> {

    public void addToStack(Fragment fragment) {
        if (!topFragmentNameIs(fragment)) {
            add(fragment.getClass().getName());
        }
    }

    private boolean topFragmentNameIs(Fragment fragment) {
        return size() > 0 && get(size() - 1).equals(fragment.getClass().getName());

    }

    public void popFragment() {
        if (size() > 0) {
            remove(size() - 1);
        }
    }

    @Nullable
    public String getTopFragmentName() {
        return size() > 0 ? get(size() - 1) : null;
    }

}
