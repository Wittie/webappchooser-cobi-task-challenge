package wittie.webappchooser.presentation.view;

import android.support.annotation.NonNull;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import wittie.webappchooser.presentation.presenter.Presenter;

public class WebAppClient extends WebViewClient {

    @NonNull
    private final Presenter presenter;

    public WebAppClient(@NonNull Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        presenter.OnPageFinishedLoading(view, url);
    }

}
