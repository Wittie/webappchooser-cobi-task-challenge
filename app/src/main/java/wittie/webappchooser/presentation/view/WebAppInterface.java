package wittie.webappchooser.presentation.view;

import android.content.Context;
import android.webkit.JavascriptInterface;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import wittie.webappchooser.R;
import wittie.webappchooser.presentation.presenter.Presenter;
import wittie.webappchooser.presentation.presenter.VibrationPresenter;

@SuppressWarnings("unused")
public class WebAppInterface {

    private Context context;
    private final Presenter presenter;

    public WebAppInterface(Context context, Presenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @JavascriptInterface
    public void showToast(String toast) {
        presenter.showDebugToast(toast);
    }

    @JavascriptInterface
    public String getDeviceInfo() {
        return presenter.getDeviceInfo();
    }

    @JavascriptInterface
    public String getTimeStamp() {
        return new SimpleDateFormat(context.getString(R.string.string_format_date), Locale.getDefault())
                .format(Calendar.getInstance().getTime());
    }

    @JavascriptInterface
    public void makePhoneVibrate(){
        if (presenter instanceof VibrationPresenter) {
            ((VibrationPresenter) presenter).makePhoneVibrate();
        }
    }


    @JavascriptInterface
    public void onError(String error){
        throw new Error(error);
    }
}
