
package wittie.webappchooser.presentation.view.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import wittie.webappchooser.R;
import wittie.webappchooser.presentation.view.FragmentsHelper;
import wittie.webappchooser.presentation.view.FragmentsStackList;
import wittie.webappchooser.presentation.view.customview.FragmentChooserBar;

public class MainActivity extends AppCompatActivity implements FragmentChooserBar.OnIconClickedListener {

    private FragmentsHelper fragmentsHelper = new FragmentsHelper();
    private FragmentsStackList fragmentsStack = new FragmentsStackList();
    private FragmentChooserBar chooserBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chooserBar = (FragmentChooserBar) findViewById(R.id.fragmentChooserBar);
        chooserBar.setOnIconClickedListener(this);
        
        onIconClicked(R.id.tab_battery);
    }

    protected void displayFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentsHelper.hideAllFragments(fragmentTransaction);

        if (!fragment.isAdded()) {
            fragmentTransaction.add(R.id.fragmentContainer, fragment);
        }

        fragmentTransaction.show(fragment);
        fragmentTransaction.commit();

        fragmentsStack.addToStack(fragment);
    }

    @Override
    public void onBackPressed() {
        if (fragmentsStack.size() < 2) {
            finish();
        } else {
            fragmentsStack.popFragment();

            final String topFragmentName = fragmentsStack.getTopFragmentName();
            displayFragment(fragmentsHelper.getFragmentWithName(topFragmentName));

            final int resIdForFragmentWithName = fragmentsHelper.getResIdForFragmentWithName(topFragmentName);
            chooserBar.selectIconWithId(resIdForFragmentWithName);
        }
    }

    @Override
    public void onIconClicked(@IdRes int clickedIconIdRes) {
        displayFragment(fragmentsHelper.getFragmentForTabResId(clickedIconIdRes));
        chooserBar.selectIconWithId(clickedIconIdRes);
    }
}
