package wittie.webappchooser.presentation.view.customview;

import android.content.Context;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import wittie.webappchooser.R;

public class FragmentChooserBar extends LinearLayout implements View.OnClickListener {

    public interface OnIconClickedListener {
        void onIconClicked(@IdRes int clickedIconIdRes);
    }

    OnIconClickedListener listener;
    View[] iconViews;

    public FragmentChooserBar(Context context) {
        super(context);
        init();
    }

    public FragmentChooserBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public FragmentChooserBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        setMinimumHeight((int) getResources().getDimension(R.dimen.bottom_bar_height));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(getResources().getDimension(R.dimen.bottom_bar_elevation));
        }

        inflate(getContext(), R.layout.custom_view_fragment_chooser_bar, this);
        setOnTabClickListener();
    }

    public void setOnIconClickedListener(OnIconClickedListener listener) {
        this.listener = listener;
    }

    public void setOnTabClickListener() {
        final int size = getChildCount();
        iconViews = new View[size];
        for (int i = 0; i < size; i++) {
            iconViews[i] = getChildAt(i);
            iconViews[i].setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onIconClicked(v.getId());
        }
    }

    public void selectIconWithId(@IdRes int clickedIconIdRes) {
        final int size = iconViews.length;
        for (int i = 0; i < size; i++) {
            if (iconViews[i].getId() == clickedIconIdRes) {
                iconViews[i].setSelected(true);
            } else {
                iconViews[i].setSelected(false);
            }
        }
    }
}
