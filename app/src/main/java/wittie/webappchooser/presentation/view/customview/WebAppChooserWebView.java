package wittie.webappchooser.presentation.view.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

import wittie.webappchooser.presentation.presenter.Presenter;
import wittie.webappchooser.presentation.view.WebAppClient;
import wittie.webappchooser.presentation.view.WebAppInterface;

public class WebAppChooserWebView extends WebView {

    public WebAppChooserWebView(Context context) {
        super(context);
        init();
    }

    public WebAppChooserWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WebAppChooserWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void init() {
        getSettings().setJavaScriptEnabled(true);
    }

    @SuppressLint({"AddJavascriptInterface"})
    public void setPresenter(Presenter presenter) {
        setWebViewClient(new WebAppClient(presenter));
        addJavascriptInterface(new WebAppInterface(getContext(), presenter), "Android");
    }
}
