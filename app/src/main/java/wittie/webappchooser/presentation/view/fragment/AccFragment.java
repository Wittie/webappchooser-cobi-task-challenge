package wittie.webappchooser.presentation.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wittie.webappchooser.R;
import wittie.webappchooser.presentation.presenter.AccPresenter;
import wittie.webappchooser.presentation.view.customview.WebAppChooserWebView;

/**
 * Created by pablo on 1/05/17.
 */

public class AccFragment extends Fragment {

    final private static String WEBAPP_ACC = "https://codepen.io/WittieApps/full/rmwMgN/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_web_view, container, false);
        WebAppChooserWebView webView = (WebAppChooserWebView) root.findViewById(R.id.webView);
        webView.setPresenter(new AccPresenter(getContext()));
        webView.setVisibility(View.INVISIBLE);
        webView.loadUrl(WEBAPP_ACC);
        return root;
    }

}
