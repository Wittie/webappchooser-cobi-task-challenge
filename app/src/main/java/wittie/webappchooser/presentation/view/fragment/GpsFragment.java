package wittie.webappchooser.presentation.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wittie.webappchooser.R;
import wittie.webappchooser.presentation.presenter.GpsPresenter;
import wittie.webappchooser.presentation.view.customview.WebAppChooserWebView;

/**
 * Created by pablo on 1/05/17.
 */

public class GpsFragment extends Fragment {

    final private static String WEBAPP_GPS = "https://codepen.io/WittieApps/full/WjjWMK/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_web_view, container, false);
        WebAppChooserWebView webView = (WebAppChooserWebView) root.findViewById(R.id.webView);
        webView.setPresenter(new GpsPresenter(getContext()));
        webView.setVisibility(View.INVISIBLE);
        webView.loadUrl(WEBAPP_GPS);
        return root;
    }

}
